import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './auth-guard.service';
import { AuthService } from './auth.service';

const appRoutes: Routes =[
  {
    path: '',
    data: { title: 'Please Sign In'},
    component: LoginComponent
  },
  { 
    path: 'home',
    canActivate: [ AuthGuardService ], 
    component: HomeComponent 
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [ AuthGuardService, AuthService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
