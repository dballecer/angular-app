import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild('loginForm', {static: false} ) loginForm: NgForm;

  public submitted = false;
  public loading = false;

  public user: string;
  private loginSubscription: Subscription;

  constructor( private authService: AuthService,
               private router: Router ) { }

  ngOnInit() : void {
    this.loading = false;
    this.submitted = false;
  }

  onSubmit( loginForm: NgForm ) {
    this.loading = true;
    this.submitted = true;

    const userMail = loginForm.value.username;
    const userPass = loginForm.value.password; 
    
    if( this.loginForm.invalid ) {
      return;
    }
    
    const loginObservable = Observable.create(
      observer => {
        setTimeout( ()=>{
          this.authService.login( userMail );
          observer.next( { userMail, userPass });
        }, 1000);
      });

    this.loginSubscription = loginObservable.subscribe(
        data => {
          this.router.navigate( ['/home'] );
        }
      );
    //this.loginSubscription.unsubscribe();
  }
}
