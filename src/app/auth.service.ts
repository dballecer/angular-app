export class AuthService {
    loggedIn = false;
    username = '';

    isAuthenticated() {
        const promise = new Promise(
            ( resolve, reject ) => {
                setTimeout( () => {
                    resolve( this.loggedIn );
                }, 800);
            }
        )
        return promise;
    }

    login( username: string ) {
        this.loggedIn = true;
        this.username = username;
    }

    logout() {
        this.loggedIn = false;
    }

    getAuthenticatedUsername() {
        return this.username;
    }
}